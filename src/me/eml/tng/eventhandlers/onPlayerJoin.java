package me.eml.tng.eventhandlers;

import java.util.List;

import me.eml.tng.TheNewGuy;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class onPlayerJoin implements Listener {
	private TheNewGuy plugin;

	public onPlayerJoin(TheNewGuy plugin) {
		this.plugin = plugin;
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void playerJoin(PlayerJoinEvent e) {
		if (Bukkit.getPlayer(e.getPlayer().getName()).hasPlayedBefore() == false) {
			Player p = e.getPlayer();
			Inventory inv = p.getInventory();
			List<String> defaultKit = plugin.getConfig().getStringList(
					"DefaultKit");
			String defaultArmor = plugin.getConfig().getString("DefaultArmor");
			Boolean broadcastWelcomeMessage = plugin.getConfig().getBoolean(
					"BroadcastWelcomeMessage");
			String initialWelcomeMessage = plugin.getConfig().getString(
					"InitialWelcomeMessage");

			for (String i : defaultKit) {
				String[] items = i.split(":");

				inv.addItem(new ItemStack(Integer.decode(items[0]), Integer
						.decode(items[1])));
			}

			if (defaultArmor.equalsIgnoreCase("leather")) {
				e.getPlayer().getInventory()
						.setHelmet(new ItemStack(Material.LEATHER_HELMET));
				e.getPlayer()
						.getInventory()
						.setChestplate(
								new ItemStack(Material.LEATHER_CHESTPLATE));
				e.getPlayer().getInventory()
						.setLeggings(new ItemStack(Material.LEATHER_LEGGINGS));
				e.getPlayer().getInventory()
						.setBoots(new ItemStack(Material.LEATHER_BOOTS));
			} else if (defaultArmor.equalsIgnoreCase("iron")) {
				e.getPlayer().getInventory()
						.setHelmet(new ItemStack(Material.IRON_HELMET));
				e.getPlayer().getInventory()
						.setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
				e.getPlayer().getInventory()
						.setLeggings(new ItemStack(Material.IRON_LEGGINGS));
				e.getPlayer().getInventory()
						.setBoots(new ItemStack(Material.IRON_BOOTS));
			} else if (defaultArmor.equalsIgnoreCase("gold")) {
				e.getPlayer().getInventory()
						.setHelmet(new ItemStack(Material.GOLD_HELMET));
				e.getPlayer().getInventory()
						.setChestplate(new ItemStack(Material.GOLD_CHESTPLATE));
				e.getPlayer().getInventory()
						.setLeggings(new ItemStack(Material.GOLD_LEGGINGS));
				e.getPlayer().getInventory()
						.setBoots(new ItemStack(Material.GOLD_BOOTS));
			} else if (defaultArmor.equalsIgnoreCase("diamond")) {
				e.getPlayer().getInventory()
						.setHelmet(new ItemStack(Material.DIAMOND_HELMET));
				e.getPlayer()
						.getInventory()
						.setChestplate(
								new ItemStack(Material.DIAMOND_CHESTPLATE));
				e.getPlayer().getInventory()
						.setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
				e.getPlayer().getInventory()
						.setBoots(new ItemStack(Material.DIAMOND_BOOTS));
			}

			initialWelcomeMessage = initialWelcomeMessage.replace("&0", "§0");
			initialWelcomeMessage = initialWelcomeMessage.replace("&1", "§1");
			initialWelcomeMessage = initialWelcomeMessage.replace("&2", "§2");
			initialWelcomeMessage = initialWelcomeMessage.replace("&3", "§3");
			initialWelcomeMessage = initialWelcomeMessage.replace("&4", "§4");
			initialWelcomeMessage = initialWelcomeMessage.replace("&5", "§5");
			initialWelcomeMessage = initialWelcomeMessage.replace("&6", "§6");
			initialWelcomeMessage = initialWelcomeMessage.replace("&7", "§7");
			initialWelcomeMessage = initialWelcomeMessage.replace("&8", "§8");
			initialWelcomeMessage = initialWelcomeMessage.replace("&9", "§9");
			initialWelcomeMessage = initialWelcomeMessage.replace("&a", "§a");
			initialWelcomeMessage = initialWelcomeMessage.replace("&b", "§b");
			initialWelcomeMessage = initialWelcomeMessage.replace("&c", "§c");
			initialWelcomeMessage = initialWelcomeMessage.replace("&d", "§d");
			initialWelcomeMessage = initialWelcomeMessage.replace("&e", "§e");
			initialWelcomeMessage = initialWelcomeMessage.replace("&f", "§f");
			initialWelcomeMessage = initialWelcomeMessage.replace("%player%", e
					.getPlayer().getName());
			initialWelcomeMessage = initialWelcomeMessage.replace("%server%",
					Bukkit.getServer().getName());

			if (broadcastWelcomeMessage == true) {
				Bukkit.getServer().broadcastMessage(initialWelcomeMessage);
			} else {
				e.getPlayer().sendMessage(initialWelcomeMessage);
			}
		}
	}
}
