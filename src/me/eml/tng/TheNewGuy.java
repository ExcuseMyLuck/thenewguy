package me.eml.tng;

import java.io.File;
import java.util.List;

import me.eml.tng.eventhandlers.onPlayerJoin;
import me.eml.tng.util.LogManager;

import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

public class TheNewGuy extends JavaPlugin implements Listener {
	public PluginDescriptionFile pdFile = getDescription();
	private File configFile = new File("plugins/TheNewGuy/config.yml");

	@Override
	public void onEnable() {
		getServer().getPluginManager().registerEvents(new onPlayerJoin(this),
				this);
		
		doesConfigExist();

		LogManager.logEnable();
	}

	@Override
	public void onDisable() {
		LogManager.logDisable();
	}

	public void doesConfigExist() {
		if (!(configFile.exists())) {
			try {
				getConfig().set("BroadcastWelcomeMessage", true);

				getConfig().set("InitialWelcomeMessage",
						"&bWelcome &c%player% &bto the server!");

				getConfig().set("DefaultArmor", "iron");

				List<String> defaultKit = getConfig().getStringList(
						"DefaultKit");
				defaultKit.add("276:1");
				defaultKit.add("277:1");
				defaultKit.add("278:1");
				defaultKit.add("279:1");
				getConfig().createSection("DefaultKit");
				getConfig().set("DefaultKit", defaultKit);
				saveConfig();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}